import schedule


def test(a, b):
    print(a+b)


def main():
    a = test(4, 5)
    schedule.every(3).seconds.do(a)

    while True:
        schedule.run_pending()


if __name__ == '__main__':
    main()
