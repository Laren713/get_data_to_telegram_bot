import requests
import json
from bs4 import BeautifulSoup
import time
import schedule


def get_products():

    url = 'https://rozetka.com.ua/ua/food_for_cats/c1461212/producer=purina-felix,purina-friskies;sell_status=available,limited;seller=rozetka/'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
    }

    req = requests.get(url=url, headers=headers)
    src = req.text

    with open('index.html', 'w', encoding='utf-8-sig') as file:
        file.write(src)

    with open('index.html', encoding='utf-8-sig') as file:
        src = file.read()

    soup = BeautifulSoup(src, 'lxml')
    all_products_hrefs = soup.find_all(class_="catalog-grid__cell catalog-grid__cell_type_slim ng-star-inserted")

    all_products_dict = {}
    for item in all_products_hrefs:
        item_name = item.find('span', class_='goods-tile__title').text
        item_href = item.find('a', class_='goods-tile__picture ng-star-inserted').get('href')
        all_products_dict[item_name] = item_href

    with open('products_dict.json', 'w', encoding='utf-8-sig') as file:
        json.dump(all_products_dict, file, indent=4, ensure_ascii=False)

    with open('products_dict.json', encoding='utf-8-sig') as file:
        all_products = json.load(file)

    count = 1
    products_list = []

    for product_name, product_href in all_products.items():
        req = requests.get(product_href)
        src = req.text

        with open(f'data_1/product_{count}.html', 'w', encoding='utf-8-sig') as file:
            file.write(src)

        with open(f'data_1/product_{count}.html', encoding='utf-8-sig') as file:
            src = file.read()

        soup = BeautifulSoup(src, 'lxml')

        name = product_name
        old_price = soup.find('p', class_='product-prices__small ng-star-inserted').text.replace(' ', '')
        new_price = soup.find('p', class_='product-prices__big product-prices__big_color_red').text
        href = product_href

        products_list.append({
            'name': name,
            'old_price': old_price,
            'new_price': new_price,
            'href': href
        })

        print(f'Processed products: #{count}')
        count += 1
        time.sleep(3)

    with open('all_products_dict.json', 'w', encoding='utf-8-sig') as file:
        json.dump(products_list, file, indent=4, ensure_ascii=False)

    print(f'[INFO] Collecting products is completed')


def main():

    schedule.every().day.at('21:48').do(get_products)

    while True:
        schedule.run_pending()


if __name__ == '__main__':
    main()
