import json
from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.markdown import hunderline, hlink
from aiogram.dispatcher.filters import Text
from config import token


bot = Bot(token=token, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot)


@dp.message_handler(commands='start')
async def start(message: types.Message):
    start_buttons = ['Felix', 'Friskies']
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*start_buttons)

    await message.reply('Hey what`s up!\nHere are something you will may buy!', reply_markup=keyboard)


@dp.message_handler(Text(equals='Felix'))
async def get_all_products(message: types.Message):
    with open('all_products_dict.json', encoding='utf-8-sig') as file:
        products_dict = json.load(file)
    for item in products_dict:
        if 'Felix' in item["name"]:
            products = f'Old price: {hunderline(item["old_price"])}\n' \
                       f'New price: {hunderline(item["new_price"])}\n' \
                       f'{hlink(item["name"], item["href"])}'

            await message.answer(products)


@dp.message_handler(Text(equals='Friskies'))
async def get_all_products(message: types.Message):
    with open('all_products_dict.json', encoding='utf-8-sig') as file:
        products_dict = json.load(file)
    for item in products_dict:
        if 'Friskies' in item["name"]:
            products = f'Old price: {hunderline(item["old_price"])}\n' \
                       f'New price: {hunderline(item["new_price"])}\n' \
                       f'{hlink(item["name"], item["href"])}'

            await message.answer(products)

if __name__ == '__main__':
    executor.start_polling(dp)